import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Match, Miss } from 'react-router';
import 'font-awesome/css/font-awesome.css';

import App from './App';
import Landing from './Components/Landing';
import Test from './Components/Test';
import Auth from './Components/Auth';
import Packages from './Components/Packages';
import './index.css';

const Root = () => {
	return (
		<BrowserRouter>
			<div>
				<Match exactly pattern="/cover" component={Landing} />
				<Match exactly pattern="/" component={App} />
				<Match exactly pattern="/test" component={Test} />
				<Match exactly pattern="/packages" component={Packages} />
				<Match exactly pattern="/signup" component={Auth} />
				<Match exactly pattern="/signin" component={Auth} />
			</div>
		</BrowserRouter>

	)
}
ReactDOM.render(
  <Root />,
  document.getElementById('root')
);
