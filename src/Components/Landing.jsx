import React, { Component } from 'react';
import './Landing.css';


class Landing extends Component {
    render() {
        return (
            <div className="homepage-hero-module">
                <div className="video-container">
                    <video autoPlay loop className="fillWidth">
                        <source src="../../../Ma-Vibes/MP4/Ma-Vibes.mp4" type="video/mp4" />Your browser does not support the video tag. I suggest you upgrade your browser.
                        <source src="../../../Ma-Vibes/WEBM/Ma-Vibes.webm" type="video/webm" />Your browser does not support the video tag. I suggest you upgrade your browser.
                    </video>
                    <div className="poster hidden">
                        <img src="PATH_TO_JPEG" alt="wha"/>
                    </div>
                </div>
            </div>
        )
    }
}

export default Landing;