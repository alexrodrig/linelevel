import React, { Component } from 'react';
import './Comments.css';


class Comments extends Component {
	render() {
		const comments = [0,1,2,3,4,5,6,7,8,9];
		return (
			<section className="comment-card">
        {comments.map((comment, i) => (
          <div className="comment-box">{i}</div>
        ))}
      </section>
		)
	}
}

export default Comments;