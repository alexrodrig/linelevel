import React, { Component } from 'react';
import './App.css';
import base from './base';

import Tracks from './Components/Tracks';
import Header from './Components/Header';
import Showcase from './Components/Showcase';
import Packages from './Components/Packages';
import Sidebar from './Components/Sidebar/Sidebar';

class App extends Component {
	constructor () {
		super();
		this.state ={
			audioSrc: ''
		}
	}
	pushUrl (url) {
		base.push('/tracks', {
			data: {title: 'Gwap', src: url}
		})
	}
	uploadFile (result) {
		const ref = base.storage().ref();
		const trackList = ref.child('/tracklist');
		trackList.put(result).then((snapshot)=>{
			this.pushUrl(snapshot.downloadURL);
		})

	}
	handleFiles (e) {
		const files = e.target.files;
		const reader = new FileReader();
		// reader.onload = (result) => {
		// 	console.log(result);
		// }
		for(let i = 0; i < files.length; i++){
			this.uploadFile(files[i]);
		}
	}
	handlePlay(src) {
		this.setState({audioSrc: src});
	}
  render() {
    return (
      <div className="App">
      	<Sidebar />
      	<div className="main-content">
	      	<Header />
	      	<Packages />
      	</div>
      </div>
    );
  }
}

/*<div className='outer-flex-container'>
		     <div className='inner-flex-container'>
		          <Packages />
		     </div>
				</div>
 /* <Tracks handlePlay={this.handlePlay.bind(this)}/>
        <div className="audio-container">

	        <label className="file-container">Upload
	        <input ref={(input) =>this.filesInput = input} type="file" name="file" id="file" multiple onChange={this.handleFiles.bind(this)} />
	        </label>
	        <audio className="audio-player" controls src={this.state.audioSrc}></audio>
	        <span className ="audio-bucks">
	        <h3>Book your Slot</h3>
	        </span>
	      </div> */
export default App;
