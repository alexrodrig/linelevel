import React, { Component } from 'react';
import './Header.css';

class Header extends Component {

  render() {
    return (
      <div className="siteHeader">
        <div className="siteHeader__section">
          <div className="siteHeader__item siteHeaderLogo">
            <div className="fa fa-inbox"></div>
          </div>
          <div className="siteHeader__item siteHeaderButton is-site-header-item-selected">Artist</div>
          <div className="siteHeader__item siteHeaderButton">Producer</div>
          <div className="siteHeader__item siteHeaderButton">Engineer</div>
        </div>
        
        <div className="siteHeader__section">
          <div className="siteHeader__item siteHeaderButton">Test</div>
          <div className="siteHeader__item siteHeaderButton">Test</div>
        </div>
      </div>
    )
  }
}

export default Header;