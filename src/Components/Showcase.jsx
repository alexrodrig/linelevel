import React, { Component } from 'react';
import './Showcase.css';

class Header extends Component {

  render() {
    return (
      <div className="showcase">
        <div className="item">
        <img alt="hey" src="https://upload.wikimedia.org/wikipedia/en/a/ae/The_Marshall_Mathers_LP.jpg" />
        </div>
        <div className="item">
        <img alt="hey" src="https://i1.sndcdn.com/artworks-000073993690-b04fmt-t500x500.jpg" />
        </div>
        <div className="item">
        <img alt="hey" src="https://upload.wikimedia.org/wikipedia/en/a/ae/The_Marshall_Mathers_LP.jpg" />
        </div>
        <div className="item">
        <img alt="hey" src="https://upload.wikimedia.org/wikipedia/en/a/ae/The_Marshall_Mathers_LP.jpg" />
        </div>
      </div>
    )
  }
}

export default Header;