import React, { Component } from 'react';
import './Card.css';

class Card extends Component {

	render() {
		return (
      <div className="card cardGroup__card">
        <div className="card__description cardGroup__cardDescription">
          <div className="icon fa fa-thumbs-o-up card__descriptionIcon"></div>
          <div className="card__descriptionText">TBD</div>
        </div>
        <div className="card__price">{this.props.title}</div>
      </div>
    )
	}
}

export default Card;