import React, { Component } from 'react';
import './Sidebar.css';

class Sidebar extends Component {

	render() {
		return (
    <div className="sidebar">
    <header className="sidebarSection">
      <div className="currentUser">
        <div className="avatars">
        </div>
        <span className="displayNames">
         <span className="userDisplayName">Alex Rodriguez</span>
         <span className="brandDisplayName">al3xelias@gmail.com</span>
        </span>
      </div>
    </header>
    <section className="sidebarSection">
      <a className="button">New Playlist</a>
      <a className="button">New Playlist</a>
    </section>
    </div>
    )
	}
}

/*<i className="fa fa-bars"></i>
<i className="fa fa-home"></i>
<i className="fa fa-search"></i>
<i className="fa fa-volume-up"></i>
<i className="fa fa-user"></i>
<i className="fa fa-spotify"></i>
<i className="fa fa-cog"></i>
<i className="fa fa-soundcloud"></i> */
export default Sidebar;