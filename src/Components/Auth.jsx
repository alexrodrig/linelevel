import React from 'react';
import base from '../base';
import { Link } from 'react-router';

class Authenticate extends React.Component {
	constructor() {
		super();
		this.accessHandler = this.accessHandler.bind(this);
		this.authHandler = this.authHandler.bind(this);
		this.state = {
			access: '',
			error: ''
		};
	}
	componentWillMount () {
		base.onAuth((user) => {
			if(user !== null) {
				if(user.displayName === null) {
					this.context.router.transitionTo('/cover');
				} else {
					this.context.router.transitionTo('/');
				}
			} else {
				const path = this.props.pathname.slice(1);
				this.setState({access: path})
			}
		})
	}
	accessHandler (e) {
		e.preventDefault();

		const credentials = {
			email: this.email.value,
			password: this.password.value
		}

		if(this.props.pathname === '/signup'){
			base.createUser(credentials, this.authHandler);
		}

		if(this.props.pathname === '/signin'){
			base.authWithPassword(credentials, this.authHandler);
		}

		this.signUpForm.reset();
	}
	authHandler (error, user) {
	  if(error) {
	  	this.setState({error: error.message});
	  } else if(this.props.pathname === '/signup') {
	  	this.context.router.transitionTo('/creator');
	  } 
	}
	render() {
		const linkPath = this.state.access === 'signup' ? 'signin' : 'signup';
		return (
			<div className="auth-container">
				<form ref={(input) => this.signUpForm = input} className="auth-form" onSubmit={(e) => this.accessHandler(e)}>
					<input ref={(input) => this.email = input} type="text" placeholder="Email" />
					<input ref={(input) => this.password = input} type="password" placeholder="Password" />
					<button type="text">{this.state.access}</button>
					<h3><Link to={`/${linkPath}`}>{linkPath}</Link></h3>
				</form>
				<span>{this.state.error}</span>
			</div>
		)
	}
}

Authenticate.contextTypes = {
	router: React.PropTypes.object
}
export default Authenticate;