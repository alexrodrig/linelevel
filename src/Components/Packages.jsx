import React, { Component } from 'react';
import './Packages.css';

import Card from './Card.jsx';

class Packages extends Component {

	render() {
		return (
			<div className="packages">
				<div className="cardGroup">
					<Card title="Artist"/>
					<Card title="Engineer"/>
					<Card title="Producer"/>
					<Card title="$300" />
					<Card title="Promotion"/>
				</div>
				<div className="cardGroup">
					<Card title="Artist"/>
					<Card title="Engineer"/>
					<Card title="Producer"/>
					<Card title="$300" />
					<Card title="Promotion"/>
				</div>
				<div className="cardGroup">
					<Card title="Artist"/>
					<Card title="Engineer"/>
					<Card title="Producer"/>
					<Card title="$300" />
					<Card title="Promotion"/>
				</div>
				<div className="cardGroup">
					<Card title="Artist"/>
					<Card title="Engineer"/>
					<Card title="Producer"/>
					<Card title="$300" />
					<Card title="Promotion"/>
				</div>
				<div className="cardGroup">
					<Card title="Artist"/>
					<Card title="Engineer"/>
					<Card title="Producer"/>
					<Card title="$300" />
					<Card title="Promotion"/>
				</div>
			</div>
    )
	}
}
// <img alt="hey" src="https://upload.wikimedia.org/wikipedia/en/a/ae/The_Marshall_Mathers_LP.jpg" />
// 	<img alt="hey" src="https://i1.sndcdn.com/artworks-000073993690-b04fmt-t500x500.jpg" />
// <p>Quote, Schedule, budget related- have a money pool?
// 								pay site per listed service, pay doesnt go out until job is complete</p>

/* <ul className="credits">
		<li>Production: Paulie Gwap</li>
		<li>Co-Production: Hammy Ham</li>
		<li>Engineering: Tom Layne, Ralph Dawson</li>
		<li>Mix: Tom Layne at Barcode Studios</li>
		<li>Upcoming Performances</li>
</ul>*/
export default Packages;