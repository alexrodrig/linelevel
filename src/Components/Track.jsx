import React, { Component } from 'react';
import './Tracks.css';


class Track extends Component {
	render() {
		const {track, onPlay} = this.props;
		return (
				<div className="track-card" onClick={onPlay}>
		      <div className="track-image">
		        <img alt="hey" src="https://upload.wikimedia.org/wikipedia/en/a/ae/The_Marshall_Mathers_LP.jpg" />
		      </div>
		      <div className="track-info">
		        <h5>{track.title}- Release the Kraggen</h5>
		        <h5 className="share">[Share]</h5>
		        <h5 className="save">[Save For Later]</h5>
		      </div>
	      </div>
		)
	}
}

export default Track;