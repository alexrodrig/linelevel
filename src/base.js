import Rebase from 're-base';

const base = Rebase.createClass({
	apiKey: "AIzaSyCeeUhsmvsLRJcDCIk3j3bYhoCekJ1-eIw",
  authDomain: "linelevel-2b4d8.firebaseapp.com",
  databaseURL: "https://linelevel-2b4d8.firebaseio.com",
  storageBucket: "gs://linelevel-2b4d8.appspot.com"
});

export default base;