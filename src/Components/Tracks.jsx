import React, { Component } from 'react';
import './Tracks.css';
import Track from './Track';
import Comments from './Comments';
import base from '../base';

class Tracks extends Component {
  constructor() {
    super();
    this.state = {
      tracks: []
    }
  }
  componentWillMount(){
    this.ref = base.syncState(`/tracks`,
    {
      context: this,
      state: 'tracks',
      asArray: true,
      queries: {
        limitToLast: 4
      }
    })
  }
  componentWillUnmount(){
    base.removeBinding(this.ref);
  }
	render() {
    console.log(this.state.tracks)
		return (
  		<div className="interface">
        {this.state.tracks.map((item, i) => (
          <div className="track-container">
            <span className="track-number">{i}</span>
            <Track track={item} id={i} onPlay={() => this.props.handlePlay(item.src)}/>
          </div>
        ))}
      </div>
    )
	}
}

export default Tracks;